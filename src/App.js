import "./App.css";
import Header from "./components/Header";
import axios from "axios";
import { useEffect, useState } from "react";
import PunkList from "./components/PunkList";
import Main from "./components/Main";

function App() {
  const [punkListData, setPunkListData] = useState([]);
  const [selectedPunk, setSelectedPunk] = useState(0);

  useEffect(() => {
    const getMyNfts = async () => {
      const openseaData = await axios.get(
        //set order direction for anable using token_id for select
        "https://testnets-api.opensea.io/api/v1/assets?asset_contract_address=0x1fbc466a58999a9fC62e77891076D8034eF20019&order_direction=asc&offset=0&limit=20"
      );
      console.log(openseaData.data.assets);
      setPunkListData(openseaData.data.assets);
    };

    getMyNfts();
  }, []);

  return (
    <div className="App">
      <Header />

      {punkListData.length > 0 && (
        <>
          <Main punkListData={punkListData} selectedPunk={selectedPunk} />
          <PunkList
            punkListData={punkListData}
            setSelectedPunk={setSelectedPunk}
          />
        </>
      )}
    </div>
  );
}

export default App;
